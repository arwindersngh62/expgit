import datetime,os

from config import getInstrumentHandler,getScanner,experimentName,axes,startCoords,stopCoords,step_size
from utils.action import Action
import h5py
import numpy as np
import shutil





def makeDir(filepath:'str',count:'int') -> 'str':
    '''
    Creates a directory and if the directory already exists, it creates a directory with t numbered name to avoid conflict
    #Input args : path to directory, current count
    #output : new path to file 

    '''
    try:                                  # if there is no naming conflict already
        filePath = f"{filepath}-{count}"
        os.mkdir(filePath)
    except:                               # if threre is a naming conflict then increase the count
        filePath = makeDir(filepath,count+1)
    return filePath




class ExperimentMetadata():
    # Keeps track of all the metadata for the experiment, this includes:
    # creating directory for the new experiment, copy the contents of the config file into the new file
    # copy all the relevent data processing files 
    # creating and providing referece to the hdf5 file      
    def __init__(self,experimentName:'str') -> 'None':
        # create new directory
        self.experimentName = experimentName
        self.directory = os.path.join(os.path.dirname(os.path.realpath(__file__)),os.path.join('outputs',experimentName+'-'+str(datetime.datetime.now().date())))
        self.directory = makeDir(self.directory,0)

        self._mainModuleFile = os.path.join(self.directory,'MainExp.txt') # create log file
        self._configFile = self.create_config()                           # copy required files
        # set up hdf5 file
        self._fileNameh = os.path.join(self.directory,'OscData.hdf5')     
        fileh = h5py.File(self._fileNameh, "a")
        config_data = [np.array(startCoords),np.array(stopCoords),np.array(step_size)]
        config_data = np.array(config_data)
        fileh.create_dataset(f"config", config_data.shape, dtype='f',data = config_data)
        string_dt = h5py.special_dtype(vlen=str)
        #print(axes)
        config_data = np.array(axes,dtype=object)
        fileh.create_dataset(f"axes", config_data.shape, dtype=string_dt,data = config_data)
        fileh.close()
    
    def create_config(self) ->'str':
    ############add files that need to be copied to new folder of experiment#########
        files_to_copy = ["replayNew.py","replay.bat","Context.py","DataManager.py","DataHandler.py","DataVisualizer.py"]
        for file in files_to_copy:
            shutil.copyfile(os.path.join("data_processing",file), os.path.join(self.directory,file))
    ###############copying the config file###########################################
        filename = os.path.join(self.directory,"config.py")
        file = open(filename,"a")
        configfile = open("config.py","r")
        ignore_lines = ["","from","def","scanner"]
        file.writelines("from ctypes import * \n")
        for i in configfile.readlines():
            if i.split(" ")[0] not in ignore_lines:
                file.writelines(i)
        #file.writelines("{"*"*10} \n")
        file.close()
        configfile.close()
        return filename

class ExperimentOutput():
    def __init__(self,experimentName):
        self._metadata = ExperimentMetadata(experimentName)
  
###   This is where all the output is handled and this is where we change the data logging and storing modules
    def addStepData(self,action,output,coords):
        # Writes the output to hdf5 file
        actionData = f'TimeStamp:{datetime.datetime.now()}, Instrument:{action._instName}, Action Type:{action._actionType}, Action Data:{action._actionData}, coords are {coords}'
        if output[0]:
            actionData= actionData+': Success'
            if action._actionType  == "GET_DATA":   # only action data corresponding to the get-Data is added to the hdf5 file
                actionData= actionData+'File:GetDataOsc'+str(datetime.datetime.now().date())+'_'
                outputData = output[1][0]
                #print(len(outputData[0]))
                #print(len(outputData[1]))
                h5data = np.array([outputData[0],outputData[1]])
                fileh = h5py.File(self._metadata._fileNameh, "a")
                print(coords)
                fileh.create_dataset(f"{coords}", h5data.shape, dtype='f',data = h5data)
                fileh.close()
        else:
            actionData= actionData+': Failure'
        file = open(self._metadata._mainModuleFile,"a")
        #logging data
        file.writelines(actionData+'\n')
        file.close()
    

        



class Experiment():

# Actual experiment method this method calls all other methods and then performs actions one at a time.
    def __init__(self,experimentName,experimentMetaData=None,verbose=False):
        self._metaData = experimentMetaData                   
        self._output = ExperimentOutput(experimentName)        
        self._actions = []                         
        self.instHandler = getInstrumentHandler()
        self.scanner = getScanner()
        self._currCoord = [0]*self.scanner._axis_num
        self._verbose = verbose                              # make this true if you want to print detailed logs

    def loadScanner(self):                                   # use scanner to create all sets of actions
        self._actions = self.scanner.compile()
        #print(self._actions)
    
    def printActions(self):                                  # print all the actions 
        for action in self._actions:
            print(action)

    def addAction(self,instName,actionType,actionData):      # method to add custom actions to the actions
        self._actions.append(Action(instName,actionType,actionData))

    def addActionFunction(self,actionfunc):                  # use a different method than the one used scanner to create actions
        self._actions = actionfunc()

    def runExperiment(self):                                 # runs the experiment           
        totalActions = len(self._actions)
        currActionCount = 0
        #print(totalActions)
        first=True                                            # used for printing time information, start time needs to be captured in the first iteration
        for currAction in self._actions:
            #try:## add level of failure as well , by returning strings
            if first:
                timestart = datetime.datetime.now()           # start time
            currOutput  = self.instHandler.runAction(currAction)       # get outputs from lower layer
            if currAction._instName == self.scanner.scanStage:           # stage data is used to get coordinate 
                index = self.scanner.getnameIndex(currAction._actionData[0]) 
                self._currCoord[index]  = currAction._actionData[1][0]
            if self._verbose==True:
                print(f'Output for action {currAction._actionType} with data {currAction._actionData} is {currOutput},coords are:{self._currCoord}')
            currActionCount+=1

            self._output.addStepData(currAction,currOutput,self._currCoord)
            if int(currActionCount%(totalActions*0.05)) == 0:     # used for timing information
                timenow = datetime.datetime.now()
                timeDone = timenow - timestart
                print(timeDone,(totalActions-currActionCount)/currActionCount)
                first = False

                print(f'Percent Completed: {int( (currActionCount/totalActions)*100)} %;  Estimated time remaining: {((totalActions-currActionCount)/currActionCount)*timeDone} ')
            #except:
              #  print(f"Action {currAction._actionType} for instrument {currAction._instName} failed to execute" )

if __name__=="__main__":
    exp = Experiment(experimentName,verbose=True)
    exp.loadScanner()
    exp.runExperiment()