from ctypes import * 
from utils.modules import Scanner
from hardware.instrumentComponent import instrumentHandler
from collections import Counter
'''
This is the configuration file for the experiments.
If you do not want any line to be considered for the purpose of an expriment, you can commentit out by 
putting a hash('#') at the start of the line.

First Thing to define is the name of the experiment. This is used to create a directory for the data files 
of your experiment. This can be any name but in general try to AVOID SPACES in the name. Also, it is very 
helpful to have descriptive and short name for the experiment
'''
experimentName = 'ScanTest'

'''
This is the section where the instruments can be added or removed for the scans.
Each instrument needs to be configured by adding the correct configuration data. 
Instruments have different configuration data like address, serial number.
Every instrument has a type and a name apart from along with specific configuration data like
address, serial numbers etc. 
In general, on the left of '=' sign is the variable name used by python and should not be changes. On the 
right side of  '='  is the configurable data and can be changed the as described. 
'''
instruments = []
'''
This section defines the oscilloscope that is to be used in the scan. The configuration of an oscilloscope 
currently supported required three main configuration data. These are name, model and ip_address of the scope
'''
#model = 'Tektronix_DPO4102B'
model = 'dummyReader'
name= 'ScanOsc'
ip_address = '192.168.1.5'
data = [model,name,{'ipAddress':ip_address}] 
instruments.append(data)



'''
In this section the stages to be used for the scan are configured, there is an option to collect all satges 
to one n-dimensional stage,for this configuration data for each of the stages need to be configured. This 
can be done adding or removing lines from the following. 
NOTE: Take care not to repeat any stages.
The section below is for stage data section. Add or Remove lines by adding  a hashtag '#' symbol at the start 
of the line(please do not delete lines from below.). All lines with a hashtag will not be considered and treated 
as comment. 
'''
stageData= []
'''
The following stages are of type THorlabs Kinesis.Each of the following lines defines a stage congifuration 
and the values inside the square brackets can be changed to change configuration
The configurations are explained below:
'''

stageData.append(['Y-Stage','linear',c_char_p(b'27256338'),'Y',False])
stageData.append(['X-Stage','linear',c_char_p(b'27255894'),'X',False])
stageData.append(['Z-Stage','linear',c_char_p(b'27004364'),'Z',False])
stageData.append(['rotating','rotational',c_char_p(b'27005004'),'R1',False])
stageData.append(['wiregrid','field',c_char_p(b'27255354'),'R2',False])
#stageData.append(['wiregrid2','rotational',c_char_p(b'27255353'),'R3',True])

'''
Following is the overall configuration of all the all the stages , the kinesis stages are the only onne currently supported
The configuration data means:

'''
stage_name = 'ScanStage'
#stage_type = 'KinesisController'
stage_type = 'dummyMover'
data = [stage_type,stage_name,{'createStages':True,'stageData':stageData}]  

instruments.append(data)



'''
This is the configuration for the scan that needs to be performed. Each of the configuration values are :

'''
moduleType = '2DScan'    
axes = ['X','Y','Z','R1','R2']       #definition found in line 88ff  ,'Z', 'R1'    LEFTMOST APRAMETERS TURNS LAST
startCoords = [15.5,0,0,50,50]      
stopCoords = [17.5,10,30,60,60]     
step_size =  [0.5,1,10,10,10]  

scanStage = 'ScanStage'  
scanReader = 'ScanOsc'       
stageSettleTime = 0.5      
resolution = 100        
scanner = Scanner(axes,startCoords,stopCoords,step_size,scanStage,scanReader,stageSettleTime,resolution,stageData)
###########################Do not change anything after this point##############################
def getInstrumentHandler():
    '''
        This is an intrument handler
    '''
    instHandler = instrumentHandler()
    check_for_duplicates(instruments,1,'instruments-names')
    check_for_duplicates(stageData,0,'stages-names')
    check_for_duplicates(stageData,3,'sage-axis')
    for inst in instruments:
        instHandler.add_instrument(inst)
        pass
    return instHandler

def getScanner():
    global scanner
    return scanner  


# a list of lists having dupliction at any index
def check_for_duplicates(data,check_index,name):
    new_data = list(map(lambda x:x[check_index],data))
    count = [key for key in Counter(new_data).keys() if Counter(new_data)[key]>1]
    if len(count)>0:
        raise KeyError(f"One or more {name} names have been duplicated {count}")
