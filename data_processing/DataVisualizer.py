from ctypes import FormatError
import matplotlib.pyplot as plt
import seaborn as sns



class DataVisualizer:
    def __init__(self,dataHandler):
        self.dataHandler = dataHandler

    def OneDPlot(self,axesNames,axesValues):
        data = self.dataHandler.get1DSlice(axesNames,axesValues)
        
        #self.dataHandler.setAccumulator(1)
        formattedData = []
        formattedCoords = []
        for i in data[0]:
            formattedData.append(i[0])
        for j in data[1]:
            formattedCoords.append(j)
        plt.plot([x[data[3]] for x in formattedCoords],formattedData)
        axis_name = data[2]
        if axis_name[0]=="R":
            plt.xlabel(f"Measurements for {axis_name}-Axis (degrees)")
        else:
            plt.xlabel(f"Measurements for {axis_name}-Axis (mm)")

        plt.ylabel(f"Accumulator: {self.dataHandler.acc.name}")
        plt.show()

    def heatMap(self,axesNames,axesValues):
        #self.dataHandler.setAccumulator(1)
        #print(self.dataHandler.acc)
        data = self.dataHandler.get2DSlice(axesNames,axesValues)
        formattedData = []
        formattedCoordsx = []
        formattedCoordsy = []
        print(data[2])
        for data1 in data[0]:
            temp = []

            formattedData.append(data1[0])
        #formattedCoordsy = [x[data[3][0]] for x in data[1]]
        for coord in data[1]:
            #print(coord)
            self.conditionalAppend(formattedCoordsx,coord[data[3][0]])
            self.conditionalAppend(formattedCoordsy,coord[data[3][1]])
        #for coord in data[1][0]:
           # formattedCoordsx.append(coord[1])
        dataf = []
        for i in range(len(formattedCoordsx)):
            temp = []
            k=0
            for j in range(len(formattedCoordsy)):
                temp.append(formattedData[k*i+j])
            dataf.append(temp)
        print(formattedCoordsy)
        print(formattedCoordsx)

        #######################3
        # fig, ax = plt.subplots()
        # ax.imshow( formattedData , cmap = 'autumn' , interpolation = 'nearest' )
        x_axis_name = data[2][0]
        y_axis_name = data[2][1]
        # if x_axis_name[0] == 'R': 
        #     ax.set_xlabel(f"Axis : {x_axis_name}(degrees)")
        # else:
        #     ax.set_xlabel(f"Axis : {x_axis_name}(mm)")
        # if y_axis_name[0] == 'R':
        #     ax.set_ylabel(f"Axis : {y_axis_name}(degrees)")
        # else:
        #     ax.set_ylabel(f"Axis : {y_axis_name}(mm)")
        # 
        # c = ax.pcolormesh(formattedData)
        # plt.title( "2-D Heat Map" )
        # fig.colorbar(c)
        # plt.show()
        ##################################
        plt.style.use("seaborn")
        fig,ax = plt.subplots()
        print(dataf)
        heat_map = sns.heatmap( dataf, linewidth = 0 , annot = False) 
        plt.title( "HeatMap using Seaborn Method" )
        
        ax.set_xticklabels(formattedCoordsy)
        ax.set_yticklabels(formattedCoordsx)
        plt.show()
        #plt.imshow( formattedData,cmap="rainbow")
        #plt.show()
        #print(data)

    def conditionalAppend(self,mainList,value):
        if value in mainList:
            pass
        else:
            mainList.append(value)
    
    def plotPoint(self,coord):
        data = self.dataHandler.getCompletePoint(coord)
        plt.title(f"Oscilloscope trace at point {coord}")
        plt.xlabel(f"Seconds")
        plt.ylabel(f"Volts")
        plt.plot(data[0],data[1])
        plt.show()


if __name__ == "__main__":
    from DataHandler import DataHandler
    dh = DataHandler()
    dv = DataVisualizer(dh)
    dv.heatMap([],[])
    