from typing import OrderedDict
import h5py
import numpy as np
from numpy.core.defchararray import index
from numpy.core.getlimits import iinfo
from numpy.lib.function_base import _gradient_dispatcher
from sqlalchemy import true
from config import resolution
import enum
import os
class Accumulator(enum.Enum):
    Integrate = 1
    Raw = 0
    Sum = 2
    Max = 3

class hdf5FileReader:
    def __init__(self,file_path,nested=True,maskBackground=True):
        try : #load file from file path provided  in read mode
            self._file = h5py.File(file_path, "r")
              
            self.createConfigData()                    
            print(f"Axes : {np.array(self._axes)}")
            print(f"Start Coords : {self._startCoords}")
            print(f"Stop Coords : {self._stopCoords}")
            print(f"Step Size : {self._stepSize}")
            self.accumulator = Accumulator.Raw
            self.nested = nested
            self.showBackground=False
            self.createCoordinates()
        except:
            raise FileNotFoundError
    
    # Create config data 
    # axes : axes names 
    # configData: contains start coords,endCoords,stepSize

    def createCoordinates(self):
        coordList= self._startCoords.copy()
        #for index in reversed(range(len(self._axes))):
        static = self.generateCoords(0)
        coordList = self.mapCoords(static, coordList, 0)
        print("inside generate coords")
        print(coordList)
            

    def showBackground(self):
        self.showBackground=True
    

    def createConfigData(self):
        self._axes = list(self._file.get('axes'))
        configData = self._file.get('config')
        self._startCoords = configData[0]
        self._stopCoords = configData[1]
        self._stepSize = configData[2]
    
    
    def checkInput(self,axes_names,coords,dimensions):
        if len(axes_names) != len(coords):
            raise ValueError("Number of axis names and the coords do not match")
        diff = len(self._axes) - len(axes_names)
        if diff == dimensions :
           return True
        return False


    def get1DSlice(self,axis_names,coord_value):# get data of 1D slice by providing all coordinates but one
        # check input for dmension mismatch and compatiility for this slice
        if not self.checkInput(axis_names,coord_value,1):
            raise ValueError("Number of axis names provided is not correct for this method 1D slice")
        #self.checkInput(axis_names,coord_value,1)
        indices, axis_dict = self.getIndices(axis_names,coord_value) # get the indices and an axis dict map of all the values 
        
        genCoords = self._startCoords.copy()  
        for i in indices:
            genCoords[i] = float(axis_dict[i])
        print(f"Index names are {axis_names} :{indices}, gencoords : {genCoords}" )
        # generate 1-D coordinates for the remainig coordinate
        for i in range(0,len(genCoords),1):
            if not (i in indices):
                coords = self.generateCoords(i)
                
                genCoords = self.mapCoords(genCoords, coords, i)
        return self.getSliceData(genCoords)

    def get2DSlice(self,axis_names,coord_value):# get data of 1D slice by providing all coordinates but one
        if not self.checkInput(axis_names,coord_value,2):
            raise ValueError("Number of axis names provided is not correct for this method 2D slice")
        indices, axis_dict = self.getIndices(axis_names,coord_value)
        genCoords = self._startCoords.copy()
        for i in indices:
            genCoords[i] = float(axis_dict[i])
        print(f"Index names are {axis_names} :{indices}, gencoords : {genCoords}" )
        missing = self.findMissingCoords(genCoords,indices)
        print(f"missing {missing}")
        coords = self.generateCoords(min(missing))
        genCoords = (self.mapCoords(genCoords, coords, min(missing)))
        maxIndex = max(missing)
        coordsList = []
        for i in genCoords:
            coords = self.generateCoords(maxIndex)
            genCoords = (self.mapCoords(i, coords, maxIndex))
            
            self.mapper(coordsList,self.getSliceData(genCoords))
        return coordsList,genCoords

    def get3DSlice(self,axis_names,coord_value):# get data of 1D slice by providing all coordinates but one
        if not self.checkInput(axis_names,coord_value,3):
            raise ValueError("Number of axis names provided is not correct for this method 3D slice")
        indices, axis_dict = self.getIndices(axis_names,coord_value)
        genCoords = self._startCoords.copy()
        for i in indices:
            genCoords[i] = float(axis_dict[i])
        print(f"Index names are {axis_names} :{indices}, gencoords : {genCoords}" )
        missing = self.findMissingCoords(genCoords,indices)
        print(f"missing {missing}")
        coords = self.generateCoords(min(missing))
        genCoords = (self.mapCoords(genCoords, coords, min(missing)))
        missing.remove(min(missing))
        print(f"outside loop {genCoords}")
        minIndex = min(missing)
        coordsList = []
        for i in genCoords:
            coords = self.generateCoords(minIndex)
            genCoordsa = (self.mapCoords(i, coords, minIndex))  
            maxIndex = max(missing)
            coordbList = []
            print(genCoordsa)
            for j in genCoordsa:
                coords = self.generateCoords(maxIndex)
                genCoordsb = (self.mapCoords(j, coords, maxIndex))
                self.mapper(coordbList,self.getSliceData(genCoordsb))
            self.mapper(coordsList,coordbList)
        
        return coordsList

    def get4DSlice(self,axis_names,coord_value):# get data of 1D slice by providing all coordinates but one
        if not self.checkInput(axis_names,coord_value,4):
            raise ValueError("Number of axis names provided is not correct for this method 4D slice")
        indices, axis_dict = self.getIndices(axis_names,coord_value)
        genCoords = self._startCoords.copy()
        for i in indices:
            genCoords[i] = float(axis_dict[i])
        print(f"Index names are {axis_names} :{indices}, gencoords : {genCoords}" )
        missing = self.findMissingCoords(genCoords,indices)
        print(f"missing {missing}")
        firstMin = min(missing)
        missing.remove(firstMin)
        secMin = min(missing)
        missing.remove(secMin)
        thirdMin = min(missing)
        fourthMin = max(missing)
        coords = self.generateCoords(firstMin)
        genCoords = (self.mapCoords(genCoords, coords, firstMin))
        coordsList = []
        for i in genCoords:
            coords = self.generateCoords(secMin)
            genCoordsa = (self.mapCoords(i, coords, secMin)) 
            print(genCoordsa)
            coordsLista = [] 
            for j in genCoordsa:
                coords = self.generateCoords(thirdMin)
                genCoordsb = (self.mapCoords(j, coords, thirdMin))
                coordsListb = []
                for k in genCoordsb:
                    coords = self.generateCoords(fourthMin)
                    genCoordsc = (self.mapCoords(k, coords, fourthMin))
                    self.mapper(coordsListb,(self.getSliceData(genCoordsc)))
                self.mapper(coordsLista,coordsListb)
            self.mapper(coordsList,coordsLista)
        return coordsList

    def get_all_data(self):
        all_keys = list(self._file.keys())
        output = []
        keysopt = []
        for key in all_keys:
            if not(key  == 'axes' or key == 'config' ):
                output.append(np.array(self._file.get(key)))
                keysopt.append(key)
        return keysopt,output
    
    def findMissingCoords(self,genCoords,indices):
        missing = []
        for i in range(len(genCoords)-1,-1,-1):
            if not(i in indices):
                missing.append(i)
        return missing


    def mapCoords(self,static, coordList, index):
        data = []
        print(f"input to the mapper is static:{static},coordList:{coordList} and indes:{index}")
        for i in coordList:### take the static coordinates and add one coordinate from coords list and create a new list.
            temp = static.copy()   # copy the static coordinates 
            #print(f"temp is {temp} and l ength of temp is {len(temp)}")
            temp = list(map(float,temp))    
            temp[index] = i
            data.append(temp)
        return data
        
    
    def generateCoords(self,index): ## generates a list of coordniates for a single index in the coordinates lists
        coords = []
        for i in range(int(self._startCoords[index]*resolution),int(self._stopCoords[index]*resolution+self._stepSize[index]*resolution),int(self._stepSize[index]*resolution)):
            coords.append(i/resolution)
        return coords

    
    def getIndices(self,axis_names,coords):
        ## returns the indices of the coordinates and axis dictinary mapping coorinates to indices.
        indices = []
        index_dict = {}
        for i in range(len(axis_names)):
            index = self._axes.index(bytes(axis_names[i], 'utf-8'))
            index_dict[index] = float(coords[i])
            indices.append(index)
        return indices,index_dict

    def getSliceData(self,coords):
        data = []
        if self.accumulator == Accumulator.Raw:
            for i in coords:
                temp = self.getDataPoint(i)                
                data.append(temp)
        elif self.accumulator == Accumulator.Integrate:
            for i in coords:
                temp = self.getDataPoint(i)
                data.append(np.sum(temp[1])/10000.0)
        elif self.accumulator == Accumulator.Sum:
            for i in coords:
                temp = self.getDataPoint(i)
                data.append(np.sum(temp[1]))
        elif self.accumulator == Accumulator.Max:
            for i in coords:
                temp = self.getDataPoint(i)
                data.append(np.max(temp[1]))
        return data
    
    def setAccumulator(self,accum):
        if accum == 0:
            self.accumulator = Accumulator.Raw
        if accum == 1:
            self.accumulator = Accumulator.Sum
        if accum == 2:
            self.accumulator = Accumulator.Integrate
        if accum==3:
            self.accumulator = Accumulator.Max
            

    
    def getDataPoint(self,coords):
        if type(coords) == list:
            return np.array(self._file.get(str(coords)))
        else:
            raise(f"The type of coords must be list , which is not the case with {coords}")
        

    def mapper(self,old,new):
        if self.nested:
            old.append(new)
        else:
            old+=new

    def get_all_data(self):
        all_keys = list(self._file.keys())
        print(all_keys)
        
        output = []
        keysopt = []
        for key in all_keys:
            if not(key  == 'axes' or key == 'config' ):
                output.append(np.array(self._file.get(key)))
                keysopt.append(key)
        return keysopt,output

    def saveTofile(self,data,name,dims):
        filename = name+".csv"
        file = open(filename,"a")
        for i in data:
            if dims>1:
                for j in i:
                        file.write(str(j)+",")
                        file.write("; \n")
            else:
                file.write(str(i)+",")
        file.close()
        return filename



        




    
if __name__ == "__main__":
    reader = hdf5FileReader("OscData.hdf5")
    #reader.setAccumulator(1)
    reader.getDataPoint([8.0, 13.0, 10.0, 0.0, 0.0,0.0])
    #print(reader.get_all_data()[0)
    reader.get3DSlice(['X','Y'],[8.5,13])
    ##print(reader.get4DSlice(['Y'],[13]))
    #print(reader.get_Keys())
    #print(reader.getDataFromKey(str([8, 13, 10,0, 10.0])))
    #reader.get1DSlice(['Y','X','Z'],[13 , 8.5, 