
from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
import pyqtgraph as pg 
from DataHandler import DataHandler

class Replay:
    def __init__(self,data):
        self.data = data
        self.count = 0
        self.setupWindow()
        self.run()

    def getNext(self):
        self.count+=1
        if self.count < len(self.data):
            return self.data[self.count]
        self.count = 0
        return self.data[self.count]

    def setupWindow(self):
        self.app = QtGui.QApplication([])
        self.win = pg.GraphicsLayoutWidget(show=True, title="Replay")
        self.win.resize(2000,1200)
        self.win.setWindowTitle('Replay')
        self.p6 = self.win.addPlot(title="Recorded data")     
        self.p6.addLegend()
        self.p6.setRange(yRange=[-10,10])
        self.curve = self.p6.plot(pen='y')
        self.plotItem = pg.PlotDataItem(name='dataset')
        self.p6.addItem(self.plotItem)
    
    def update(self):
        data =self.getNext()
        print(data)
        #self.curve.setData(data)

    def run(self):
        timer = QtCore.QTimer() 
        timer.timeout.connect(self.update)
        timer.start(100)
        QtGui.QApplication.instance().exec_()


dh = DataHandler()
data = dh.getallData()
print(data)
rp = Replay(data)
#rp.run()

#rp = Replay(dh.getallData())


