from msilib.schema import Error
import h5py
from matplotlib.pyplot import axes
from Context import CoordinateContext
import numpy as np
import enum
import os
import datetime
def makeDir(filepath:'str',count:'int') -> 'str':
    '''
    Creates a directory and if the directory already exists, it creates a directory with t numbered name to avoid conflict
    #Input args : path to directory, current count
    #output : new path to file 

    '''
    try:                                  # if there is no naming conflict already
        filePath = f"{filepath}-{count}"
        os.mkdir(filePath)
    except:                               # if threre is a naming conflict then increase the count
        filePath = makeDir(filepath,count+1)
    return filePath
class Accumulator(enum.Enum):
    Raw = 0
    Integrate = 1
    Max = 2
    Sum = 3

class DataHandler:
    
    def __init__(self):
        self.file = h5py.File("OscData.hdf5", "r") #Default file name for the hdf5 file is always OscData
        self.context = CoordinateContext(self.file) #creates the context for the file
        self.acc = Accumulator.Raw 


    def getallData(self):
        data = []
        for coord in self.context.allCoords:
            data.append(self.getDataPoint(coord))
        return data


    def checkInput(self,axes_names,coords,dimensions):
        if len(axes_names) != len(coords):
            raise ValueError("Number of axis names and the coords do not match")
        diff = len(self.context.axes) - len(axes_names)

        if diff == dimensions:
           return True
        return False


    def getPlaceHolder(self,axesName,axesValues):
        placeHolder = [None]*len(self.context.startCoords)
        for i in range(len(axesName)):
            index = self.context.getAxisIndex(axesName[i])
            placeHolder[index] = axesValues[i]
        return placeHolder
    

    def findMissing(self,placeHolder):
        missing = []
        for i in range(len(placeHolder)):
            if placeHolder[i] == None:
                missing.append(i)
        return missing


    def get1DSlice(self,axesName,axesValues):
        if self.checkInput(axesName,axesValues,1): 
            placeHolder = self.getPlaceHolder(axesName,axesValues)
            missing = self.findMissing(placeHolder)
            genCoords = self.context.generateCoords(missing[0])
            coords = self.context.fitPoints(placeHolder,missing[0],genCoords)
            data = []
            for coord in coords:
                data.append(self.getDataPoint(coord))
            return data,coords,str(self.context.axes[missing[0]]),missing[0]
    

    def get2DSlice(self,axesName,axesValues):
        if self.checkInput(axesName,axesValues,2): 
            placeHolder = self.getPlaceHolder(axesName,axesValues)
            missing = self.findMissing(placeHolder)
            data   = []
            genCoordsa = self.context.generateCoords(missing[0])
            genCoordsb = self.context.generateCoords(missing[1])
            coordsa = self.context.fitPoints(placeHolder,missing[0],genCoordsa)
            coordsb = []
            for coord in coordsa:
                coordsb+=(self.context.fitPoints(coord,missing[1],genCoordsb))
            for coord in coordsb:
                data.append(self.getDataPoint(coord))
            return data,coordsb,[self.context.axes[missing[0]],self.context.axes[missing[1]]],missing


    def getDataPoint(self,coords):

        if type(coords) == list:
            #print(self.file.get(str(coords)))
            data =  np.array(self.file.get(str(coords)))[1]
            #print(data)
            if self.acc==Accumulator.Raw: #raw
                data = self.removeBackground(data)
                return data
            if self.acc==Accumulator.Integrate: #integrate
                data = self.removeBackground(data)
                return [np.sum(data)/len(data)]
            if self.acc==Accumulator.Sum:# sum
                data = self.removeBackground(data)
                return [np.sum(data)]
            if self.acc==Accumulator.Max:#max
                data = self.removeBackground(data)
                return [np.max(data)]
        else:
            raise(f"The type of coords must be list , which is not the case with {coords}")

    def removeBackground(self,data):
        initAvgValue = np.sum(data[0:10])/10
        data = data -initAvgValue
        min_val = min(data)
        max_val = max(data)
        if abs(min_val) > abs(max_val):
            data = -1*data

        return data

    def getCompletePoint(self,coords):
        
        if type(coords) == list:
            data =  np.array(self.file.get(str(coords)))
            return data

        else:
            raise(f"The type of coords must be list , which is not the case with {coords}")
    
    def save1D_Bulk(self,axesName,axesValues,start,stop):
        '''
            Input Arguments:
            Variable axis: the axis which is varying 
            The values of fixed coordinates
            The range of values of the varibale axis
            Output Arguments:
            None
        '''
        placeHolder = self.getPlaceHolder(axesName,axesValues)
        missing = self.findMissing(placeHolder)
        genCoords = self.context.generateCustomCoords(missing[0],start,stop)
        coords = self.context.fitPoints(placeHolder,missing[0],genCoords)
        
        directoryPath = os.path.join(os.path.dirname(os.path.realpath(__file__)),os.path.join('EXPORT-Axis'+str(self.context.axes[missing[0]])+'-'+str(datetime.datetime.now().date())+'From-'+str(start)+'To-'+str(stop)))
        directoryPath = makeDir(directoryPath,0)
        for coord in coords:
            self.saveSingleData(coord,filePath=directoryPath)
            #self.save1Ddata(axesNames=self.context.axes,axesValues=coord,filePath=directoryPath)

        

    def save1Ddata(self,axesNames,axesValues,filePath=None):
        if self.acc == Accumulator.Raw:
            a = open(f"1DData-{axesNames}-{axesValues}.txt","a+")
            if filePath!=None:
                mainFile = os.path.join(filePath,f"1DData-{axesNames}-{axesValues}.txt")
                a = open(mainFile,"a+")
            data = self.get1DSlice(axesNames,axesValues)
            #print(data)
            for i in range(len(data)):
                value = ""
                for j in data[0][i]:
                    value+= f"{j},"
                value+=value+f"{data[1][i]} \n"
                a.write(value)
            a.close()
        else:
            a = open(f"1DData1-{axesNames}-{axesValues}-.txt","a+")
            data = self.get1DSlice(axesNames,axesValues)
            a = open(f"1DData1-{self.acc}-{data[2]}-.txt","a+")
            #print(data)
            for i in range(len(data[0])):
                value = f"{data[1][i]},{data[0][i][0]} \n"
                a.write(value)
            a.close()
        
    def save2Ddata(self,axesNames,axesValues):
        if self.acc == Accumulator.Raw:
            raise Error
        else:
            a = open(f"2DData-{self.acc.name}-{axesNames}-{axesValues}.txt","a+")
            data = self.get2DSlice(axesNames,axesValues)
            formattedData = []
            for data1 in data[0]:
                temp = ""
                for j in data1:
                    temp+=str(j[0])+","
                temp= temp[:-1]+"\n"   
                a.write(temp)
            #print(formattedData)
            #print(data[2])
            a.close()


    def saveSingleData(self,coord,filePath=None):
        data = self.getCompletePoint(coord)
        #print(data)
        a = open(f"datapoint-{coord}.txt","a+")
        if filePath!=None:
            mainFile = os.path.join(filePath,f"datapoint-{coord}.txt")
            a = open(mainFile,"a+")
        for i in range(int(len(data[0]))):
            value = f"{data[0][i]},{data[1][i]} \n"
            a.write(value)

        a.close()
        

    def get_all_raw_data(self):
        keys = self.file.keys()
        data = []
        for key in  keys:
            data.append(self.file.get(key))
        return data

        
    def setAccumulator(self,num):
        if num == 0:
            self.acc = Accumulator.Raw
        if num == 1:
            self.acc = Accumulator.Integrate
        if num == 2:
            self.acc = Accumulator.Max
        if num == 3:
            self.acc = Accumulator.Sum
            


if __name__ == "__main__":
    dh = DataHandler()
    #print(dh.getallData()) 
    #print(dh.context.axes)
    print(dh.get1DSlice(['X'],[2.0]))
    dh.get2DSlice([],[])
    #print(dh.get2DSlice(['X','Y','R1'],[8.0,13.0,0.0]))
    #print(dh.get_all_raw_data())
    #dh.save1D_Bulk([b'X'],[8.0],18,22)
    #print(dh.getCompletePoint([7.0,20.0]))
    #dh.save1Ddata(['X','Y','R1','R2'],[8.0,13.0,0.0,0.0])


