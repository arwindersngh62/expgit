from encodings import utf_8
import h5py
from config import resolution,startCoords,stopCoords,step_size,axes
class CoordinateContext:
    '''
    '''
    def __init__(self,DataFile):
        '''
            input :  The path to the hdf5 file.
        '''
        self.axes = axes # axes as stored in the hdf5 file
        #configData = DataFile.get('config')
        self.buildAxesDict()
        self.startCoords = startCoords
        self.stopCoords = stopCoords
        self.stepSize = step_size
        self.createDimensionPoints()
        self.createAllCoords()


    def createAllCoords(self):
        count = 0
        print(f"length of coords is {count}")
        allCoords = [list(self.startCoords)]
        print(f"All coordinates are {allCoords}")
        while count< len(self.axes):
            newCoord = []
            for index in range(len(allCoords)):
                newCoord+=self.fitPoints(allCoords[index],count,self.dimPoints[count])
            allCoords = newCoord.copy()
            count+=1
        self.allCoords = allCoords.copy()

        
    
    def createDimensionPoints(self):
        self.dimPoints = []
        for coord in range(len(self.axes)):
            self.dimPoints.append(self.generateCoords(coord))


    def fitPoints(self,coords,index,extensions): # put full cooord, and all extension coords that need to be fit in there and the index at which they need to be fit at
        newCoords = []
        for point in extensions:
            temp = coords.copy()
            temp[index] = point
            newCoords.append(temp)
        return newCoords

    def generateCoords(self,index): ## generates a list of coordniates for a single index in the coordinates lists
        coords = []
        #print(f"{int(self.startCoords[index]*resolution)},{int(self.stopCoords[index]*resolution+self.stepSize[index]*resolution)},{self.stepSize[index]}")
        for i in range(int(self.startCoords[index]*resolution),int(self.stopCoords[index]*resolution+self.stepSize[index]*resolution),int(self.stepSize[index]*resolution)):
            #print(i/resolution)
            coords.append(i/resolution)
        return coords

    def generateCustomCoords(self,index,start,stop):
        coords = []
        #print(f"{int(self.startCoords[index]*resolution)},{int(self.stopCoords[index]*resolution+self.stepSize[index]*resolution)},{self.stepSize[index]}")
        for i in range(int(start*resolution),int(stop*resolution+self.stepSize[index]*resolution),int(self.stepSize[index]*resolution)):
            #print(i/resolution)
            coords.append(i/resolution)
        return coords
    
    def buildAxesDict(self): # WHY do we need this ???
        ## returns the indices of the coordinates and axis dictinary mapping coorinates to indices.
        self.index_dict = {}
        for i in range(len(self.axes)):
            index = self.axes.index(self.axes[i])
            self.index_dict[self.axes[i]] = index
        return self.index_dict

    def getAxisIndex(self,axisName):
        return self.index_dict[axisName]
        

if __name__=="__main__":
    file = h5py.File("OscData.hdf5", "r")
    context = CoordinateContext(file)
    print(f"the context of the coordinates are axes:{context.axes},start:{context.startCoords},stop:{context.stopCoords},stepSize:{context.stepSize}")
    print(context.dimPoints)
    #print(context.getAxisIndex(b'R1'))
    print(context.fitPoints([1,'' ,'','' ,34],1,[1,1,1,11]))