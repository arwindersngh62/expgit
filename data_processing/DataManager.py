import enum
from multiprocessing.sharedctypes import RawArray
from os import remove
from DataHandler import DataHandler,Accumulator
from DataVisualizer import DataVisualizer

class DataManager():
    def __init__(self):
        self.dh = DataHandler()
        self.dv = DataVisualizer(self.dh)
        #self.visualMethods = [self.oneDplot,self.heatMap,]

    
    def runPrompt(self):
        print("Welcome to the data manager , select one of the following options:")
        print("1) Visualize Data \n2) Export Data")
        self.rootWords = ["visualize", "export data"]
        self.firstWords = ["visualize", "export"]
        self.secondWords = ["plot","data"]
        #TODO sanitization
        #try:
        self.type = int(input("Enter a number and press enter:"))-1
        self.firstLevelMethod()
        #except:
           # print("Invalid input")
           # self.runPrompt()
            
        


    def firstLevelMethod(self):

        print(f"Selected to {self.rootWords[self.type]}")
        print(f"What do you want to {self.firstWords[self.type]}?")
        print(f"1) One Dimensional {self.secondWords[self.type]}\n2) Two Dimensional {self.secondWords[self.type]} \n3) Single data point(oscilloscope trace at a point)")
        if self.type == 1:
            print("4) Export multiple single data points")
        #try:
        self.secondLevel = int(input("Enter a number and press enter:"))
        self.secondLevelMethod()
        #except:
        #    print("Invalid input")
        #    self.runPrompt()

    def secondLevelMethod(self):
        axes = self.dh.context.axes.copy()
        missing = len(axes) - 1 
        if self.secondLevel==1:
            missing = len(axes) - 1 
            if missing >0:
                print(f"All experimental axes are {axes}, you need to enter values of {missing} axes \nstart cooords are: {self.dh.context.startCoords} \nstop cooords are: {self.dh.context.stopCoords} \nstep size are: {self.dh.context.stepSize}")
                self.printAxes(axes)
                missingVal = input(f"Enter the index of the variable axis name that you want to {self.firstWords[self.type]} and press enter: ")
                axes.remove(axes[int(missingVal)])
            else:
                print(f"All axes are {axes}, \nstart cooords are: {self.dh.context.startCoords} \nstop cooords are: {self.dh.context.stopCoords} \nstep size are: {self.dh.context.stepSize}")
                axes = []
            inputValues = self.getInputAxes(axes)
            self.getAccumulator()
            if self.type == 0:
                self.dv.OneDPlot(axes,inputValues)
            if self.type == 1:
                self.dh.save1Ddata(axes,inputValues)
            self.start_again()
        
        if self.secondLevel==2:
            missing = len(axes) - 2 
            if missing == 4:
                print(f"All exprimental axes are {axes}, you need to enter values of {missing} axes \nstart cooords are: {self.dh.context.startCoords} \nstop cooords are: {self.dh.context.stopCoords} \nstep size are: {self.dh.context.stepSize}")
                self.printAxes(axes)
                missingVal_a = input(f"Enter the index of the variable axes name that you want to {self.firstWords[self.type]}  and press enter: ")
                remove_a = axes[int(missingVal_a)]
                axes.remove(remove_a)
                
            if missing >0:
                print(f"Exprimental axes are {axes}, you need to enter values of {missing} axes \nstart cooords are: {self.dh.context.startCoords} \nstop cooords are: {self.dh.context.stopCoords} \nstep size are: {self.dh.context.stepSize}")
                self.printAxes(axes)
                missingVal_a = input(f"Enter the index of the variable axes name that you want to {self.firstWords[self.type]}  and press enter: ")
                remove_a = axes[int(missingVal_a)]
                missingVal_b = input("Enter the index of the second variable axes name that you want to plot and press enter: ")
                remove_b = axes[int(missingVal_b)]
                axes.remove(remove_a)
                axes.remove(remove_b)
                
            if missing<1:
                print(f"All axes are {axes}, \nstart cooords are: {self.dh.context.startCoords} \nstop cooords are: {self.dh.context.stopCoords} \nstep size are: {self.dh.context.stepSize}")
                axes=[]

            inputValues = self.getInputAxes(axes)
            print(axes,inputValues)
            self.getAccumulator()
            if self.type == 0:
                print(axes,inputValues)
                self.dv.heatMap(axes,inputValues)
            if self.type == 1:
                self.dh.save2Ddata(axes,inputValues)
            self.start_again()

        if self.secondLevel==3:
            axes = self.dh.context.axes.copy()
            print(f"Experimental axes are {axes},\nstart cooords are: {self.dh.context.startCoords} \nstop cooords are: {self.dh.context.stopCoords} \nstep size are: {self.dh.context.stepSize} you need to enter coordinate values for all axes and press enter ")
            inputValues = self.getInputAxes(axes)
            acc = int(self.dh.acc.value)
            self.dh.setAccumulator(0)
            if self.type == 0:
                self.dv.plotPoint(inputValues)
            if self.type == 1:
                self.dh.saveSingleData(inputValues)
            self.dh.setAccumulator(acc)
            self.start_again()

        if self.secondLevel == 4: 
            axes = self.dh.context.axes.copy()
            dummy = [0]*len(axes)
            print(f"Experimental axes are {axes},\nstart cooords are: {self.dh.context.startCoords} \nstop cooords are: {self.dh.context.stopCoords} \nstep size are: {self.dh.context.stepSize} you need to enter coordinate values range for one axis and fix all other axes")
            self.printAxes(axes)
            a = int(input("Enter index of variable axis and press enter"))

            ## Do the following
            ## To the generate coords and fit coords method in the context, 
            ## fitCoords(list of fixed coords with the index of variable coords left blank, index of the variable axis, values
            # to input to the variable axis block)
            remove = axes[int(a)]

            axes.remove(remove)
            inputValues = self.getInputAxes(axes)
            print(f"You need to enter the range of coords for the axis {remove} that you want to export")

            start  = float(input("Enter the start coords and press enter: "))
            stop = float(input("Enter the stop coords and press enter: "))
            
            print(f"Fixed axes are {axes}")
            print(f"Variable axis is {axes}")
            print(f"input values are {inputValues}")
            print(f"Start and Stop values are : {start} , {stop}")
            self.dh.save1D_Bulk(axes,inputValues,start,stop)




                 




    
        
    def printAxes(self,axes):
        print("---------------------------------------------------")
        print("|Experimental Axes Name | Experimental axes Index |")
        print("---------------------------------------------------")
        for i in enumerate(axes):
            print(f"|           {i[1]}           |            {i[0]}            |")
            print("---------------------------------------------------")
    
    def getInputAxes(self,axes):
        val = []
        for i in axes:##TODO input sanitization
            val.append(float(input(f"Enter the coordinate value for axis {i} and press enter ")))
        return val
    
    def getAccumulator(self):
        print("Select an Accumulator and press enter")
        for acc in enumerate(Accumulator):
            if acc[0] == 0 or acc[0] == 3:
                continue
            print(acc)
        accum = input(":")
        self.dh.setAccumulator(int(accum))
    
    def start_again(self):
        again = input("enter y and press enter to start again:")
        try:
            if str(again).lower() == 'y':
                self.runPrompt()
        except:
            pass
        
        

if __name__ == "__main__":
    dm = DataManager()
    dm.runPrompt()
    #dm.printAxes(['X','Y','Z'])