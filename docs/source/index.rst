.. GitLab Pages with Sphinx documentation master file, created by
   sphinx-quickstart on Thu Jan  9 10:28:38 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

DAC
==========================================================
Hello!!! Here we are documenting the DAC software Getting data from instruments in choerent and user friendly manner. To get you started we have a small tutrorial. This is a work in progress, We will keep building, testing and uploading support for new instruments.


Requirements
==========================================================

The python module requirements needed for running these modules are provided in requirements.txt. To install the requirements as listed in the requirements.txt file, navigate to the directory where the file(requirements.txt) is stored(usually where this repository is cloned) and run following command from command line.

.. code-block:: 


      $ pip install -r requirements.txt


Apart from these there are other requirements given below:

* For Kinesis stage controller:
   `Kinesis software <https://www.thorlabs.com/newgrouppage9.cfm?objectgroup_id=10285>`_

* For pyvisa backend:
   NI VISA backend described in this `document <https://pyvisa.readthedocs.io/en/1.8/getting.html#ni-backend>`_

Intallation
=========================================================
To install, clone the DAC repository as follows:

.. code-block:: 


      $ git clone https://gitlab.com/ftnk/DAC.git

Methodology
==========================================================
The package includes two interfaces to interface and get data form intruments. These are programming and non programming interfaces.

Non Programatic Interface:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This is a user friendly way of interacting with the instruments and does not require any programming from the user.All that needs to be done is to make some changes to the config file and run the **runExperiment.bat** file. This starts the expriment and creates a new folder with the data from the experiment. After the expriment finishes, data handing can be done using **replay.bat** and **heatmap_plot.bat** file.
This :doc:`non-progg-tutorial` explains how to get started with this interface.

Programatic Interface:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
This interface allows th user to write python code to interact with instruments in a more free manner. This is for users who have beginner working knowledge of python. Most complex interactions with instruments are abstraced away and user gets a simple interface to run experiments. All interacions with instruments have been based of passing commands to a instrument controller. All such commands are listed out here and these can be used to write and handle custom experiment code.To get started on this interface, take a look at this :doc:`progg-tutorial`.
Further, Users can create support for more commands as needed by including more methods from lower layer like shown here. 


Data Handling and analysis:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
At this stage all the output from running the experiments are stored inside at ** working-directory/outputs/exepriment-name-datetime-unique_num". The directory includes:
* The output from oscilloscope in a hdf5 file
* The run logs of th experimnet
* The config file showing the configurations of the scan/experiment
* The data processing files **replay.bat** and **heatmap_plot.bat** that are based on the data of current experiment.


The long term plan is to host all the data at one place for all the users.


Current Support
=========================================================
Currently the support for following is available

Instruments
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* Oscilloscope
* Thorlabs Rotational and Linear Stages 

Scans
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* 1D,2D.. n-D Scans with static commands
* n-D scans with custom commands

Data Processing
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
* Replay of Oscilloscpe
* Heat Map of accumulated data




.. toctree::
   :maxdepth: 2
   :caption: Tutorials

   non-progg-tutorial
   prog-tutorial



Indices and tables
==================
* :ref:`genindex`
* :ref:`search`
