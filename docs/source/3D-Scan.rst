


3-D Scan:
===========================================================

* First of all name the experiment:


.. code-block:: python

        
        experimentName = 'MyFirst3Dscan'

For a 3-D scan of using the 'Tektronix_DPO4102B' oscilloscope and thorlabs Kinesis stages, the instrument configuration section should look like following:
Here the name of the oscilloscope is **ScanOsc** , this can be any name , but it must be same throughout.


* **Oscilloscope** section:

.. code-block:: python


        model = 'Tektronix_DPO4102B'
        name= 'ScanOsc'
        ip_address = '192.168.1.5'
        data = [model,name,{'ipAddress':ip_address}] 
        instruments.append(data)


* **Stages** Section

.. code-block:: python


        stageData.append(['X-Stage','linear',c_char_p(b'27256338'),'X'])
        stageData.append(['Y-Stage','linear',c_char_p(b'27255894'),'Y'])
        stageData.append(['Z-Stage','linear',c_char_p(b'27005114'),'Z'])
        #stageData.append(['rotating','rotational',c_char_p(b'27005004'),'R'])
        #stageData.append(['wiregrid','rotational',c_char_p(b'27255354'),'R'])
        
        
Here we are using two axes **X** and **Y** axes and they can be changed as long as they follow the rules as stated in :doc:`non-progg-tutorial` .
The second stage can be **'linear'** as well as **'rotational'**.stage_name
**NOTE**: Make sure you are using the correct stage serial numbers and there is not repetition of axis-names as well as stage names.
Below is the overall configuration of stages.Here the stages are being homed.

.. code-block:: python


        stage_name = 'ScanStage'
        stage_type = 'KinesisController'
        home = True
        data = [stage_type,stage_name,{'createStages':True,'stageData':stageData,'homeStages':home}]  
        instruments.append(data)


Following is the scan configuration of the scan, the scan type is **"3DScan"** (this is **NOT** user defined and must be same for all 3D scans). The axis must be same as configured above i.e "X".
The start and end coordinates of the scan are as **[8,5,1]** and **[10,7,3]** respectively and the step size is **1** which means the 
coordinates of scan will be [8,5,1],[8,5,2],[8,5,3],[8,6,1]....[10,7,3]. 
The effect of varying step_size will be the same 
as shown in :doc:`1D-Scan`

.. code-block:: python


        moduleType = '2DScan'    
        axes = ['X','Y','Z']             
        startCoords = [8,5,1]      
        stopCoords = [10,7,3]     
        step_size =  [1,1,1]  
        scanStage = 'ScanStage'    
        scanReader = 'ScanOsc'       
        stageSettleTime = 0.5      
        resolution = 100          
        scanner = Scanner(moduleType,axes,startCoords,stopCoords,step_size,scanStage,scanReader,stageSettleTime,resolution)


If the axes were defined as 

.. code-block:: python


        axes = ['Y','X','Z']


The **'Y'** axis stage will vary slowest folowed by  **'X'** axis stage and the  **'Z'** axis stage will vary slowest.


* save the config file and run runExperiment.bat file.
