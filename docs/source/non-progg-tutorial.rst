Non Programming Interface
==============================
This document contains the detailed explanantion of the the interface. For more hands on examples look at one of following:


* :doc:`1D-Scan`
* :doc:`2D-Scan`
* :doc:`3D-Scan`
* :doc:`N-D-Scan`

To get started with non programming interface first, navigate to the DAC directory. Then follow these steps:

* Open the **config.py** file and make the changes as needed which looks like:
.. code-block:: python

        from ctypes import *
        from hardware.motion_controllers import KinesisController
        from hardware.instrumentComponent import instrumentHandler
        from utils.modules import Scanner

        '''
        This is the configuration file for the experiments.
        If you do not want any line to be considered for the purpose of an expriment, you can commentit out by 
        putting a hash('#') at the start of the line.
        First Thing to define is the name of the experiment. This is used to create a directory for the data files 
        of your experiment. This can be any name but in general try to AVOID SPACES in the name. Also, it is very 
        helpful to have descriptive and short name for the experiment
        '''
        experimentName = 'MyFirstExperiment'
        '''
                                INSTRUMENTS SECTION:
        This is the section where the instruments can be added or removed for the scans.
        Each instrument needs to be configured by adding the correct configuration data. 
        Instruments have different configuration data like address, serial number.
        Every instrument has a type and a name apart from along with specific configuration data like
        address, serial numbers etc. 
        In general, on the left of '=' sign is the variable name used by python and should not be changes. On the 
        right side of  '='  is the configurable data and can be changed the as described. 
        '''
        instruments = []
        '''
                                Oscilloscope Section


* The first thing to be done is to name the experiment. This can be used to identify the experiment data at a later date. The 
data for an experiment is stored in a folder named as 'experimentName'-'date'-'unique_number' . 'unique_num' is used to uniqely 
identify th experiment folder in case a lot of experiments with same name are conducted on same day.

.. code-block:: python


        '''
        This is the configuration file for the experiments.
        If you do not want any line to be considered for the purpose of an expriment, you can commentit out by 
        putting a hash('#') at the start of the line.
        First Thing to define is the name of the experiment. This is used to create a directory for the data files 
        of your experiment. This can be any name but in general try to AVOID SPACES in the name. Also, it is very 
        helpful to have descriptive and short name for the experiment
        '''
        experimentName = 'MyFirstExperiment'
        '''


* This file contains some editable sections which needs to be configured correctly. The second editable section is 
**Instruments Section**. In this section the configuration of instruments to be used is defined. 
Make  neccessary changes to this as explained below :

.. code-block:: python

        '''
                        INSTRUMENTS SECTION:
        This is the section where the instruments can be added or removed for the scans.
        Each instrument needs to be configured by adding the correct configuration data. 
        Instruments have different configuration data like address, serial number.
        Every instrument has a type and a name apart from along with specific configuration data like
        address, serial numbers etc. 
        '''
        instruments = []
    
* This section further contains subsections for each of the instruments that are currently supported.
The first section is for oscilloscopes :

.. code-block:: python

        '''
                        Oscilloscope Section
        This section defines the oscilloscope that is to be used in the scan. The configuration of an oscilloscope 
        currently supported required three main configuration data. These are name, model and ip_address of the scope
            Explanation of each of these is :   
                model : Write the name of one of the oscilloscope models that is being used. 
                    Currently supported models are:
                        1 - 'Tektronix_DPO4102B'
                name :  This is a user defined name(any name) for the oscilloscope. This should be unique across all 
                        instrumnets , which means that no other instrument should have the same name.
                
                ip_address: The ip adress of the oscilloscope that is being used, refer the manual of the osclloscope 
                            to find out its IP adress.
            All the values of configurations must be in quotes (single or double)
        '''
        model = 'Tektronix_DPO4102B'
        name= 'ScanOsc'
        ip_address = '192.168.1.5'
        data = [model,name,{'ipAddress':ip_address}] 
        instruments.append(data)

* The next section is for stages,multiple stages can be configured by adding stagedata for each stage.Make changes as needed,
this is explained below:

.. code-block:: python


        '''
                        Stages Section
        In this section the stages to be used for the scan are configured, there is an option to collect all satges 
        to one n-dimensional stage,for this configuration data for each of the stages need to be configured. This 
        can be done adding or removing lines from the following. 
        NOTE: Take care not to repeat any stages.
        The section below is for stage data section. Add or Remove lines by adding  a hashtag '#' symbol at the start 
        of the line(please do not delete lines from below.). All lines with a hashtag will not be considered and treated 
        as comment. 
        '''
        stageData= []
        '''
        The following stages are of type THorlabs Kinesis.Each of the following lines defines a stage congifuration 
        and the values inside the square brackets can be changed to change configuration
        The configurations are explained below:
            eg. : One stage is 
                stageData.append(['X-Stage','linear',c_char_p(b'27256338'),'X'])
                here ['X-Stage','linear',c_char_p(b'27256338'),'X'] is the configuration data.
                it is mapped to follwing configuration values 
                [Name, type, serial_num, axis]
                The meaning of each of the configurations values are:
                Name: user given name for the stage, should be unique across all instuments
                type: The type of the stage can take two vaues :- 'linear' and 'rotational'
                serialNo: Serial number of the stage to be used(only for kinesis stages)
                axis :  The name of the axis that this stage represents. This can have following axis:
                        'X': The stage that handles the X-axis
                        'Y': The stage that handles the Y-axis
                        'Z': The stage that handles the Z--axis
                        'R': The stage that handles the rotational-axis.
                NOTE: There can only be one X,Y and Z axis stages. But there can be any number of R stages.
                        In case there are multiple R stages and they need to used in scan, number them as R1, R2 etc
        '''
        stageData.append(['X-Stage','linear',c_char_p(b'27256338'),'X'])


Futher some configurations are needed to be added for overall handler of all stages of the scan which is described below:

.. code-block:: python

        '''
        Following is the overall configuration of all the all the stages , the kinesis stages are the only onne currently supported
        The configuration data means:
                stage_name : User defined name for the stage controller, must be unique
                stage_type : type of the stage, currently supported type is 'KinesisController'
                home : decied whether to home the stages or not. If this is "True"(no quotes) the stages are homes, if 
                it is "False"(no quotes) the  stages are not homes

        '''
        stage_name = 'ScanStage'
        stage_type = 'KinesisController'
        home = True
        data = [stage_type,stage_name,{'createStages':True,'stageData':stageData,'homeStages':home}]  
        instruments.append(data)


* Next, define the configurations for the scan as needed. Edit the following section of the file to desired values.

.. code-block:: python

        '''
                        Scan Configuration
        This is the configuration for the scan that needs to be performed. Each of the configuration values are :
            moduleType :This decides the type of the scan to be performed it can be 1DScan,2DScan,3DScan.
            axes       :Axes that are used in the scan are described here.The first nameed axes is considred 
                        major axis and others will be considered minor in that order.
                        NOTE : There must be at least as many axes as there are dimensions in the moduleType  
            startCoords: The coordinates where the scan starts, these should always be atleast three dimensional values even if the
                        actual scan is 1-D.   
            stopCoords : The coordinates where the scan should end everything same as start coords.
            step_size  : The step size of increments for the scan coordinates.
            NOTE: startCoords, stopCoords,step_size values are positionally mapped to the axis defined in 'axes' configuration
            scanStage  : name of the instrument to use as stage for the scan.(as defined in overall configuration)
            scanReader : name of the instrument  to use as reader for the scan , here it is oscilloscope    
            stageSettleTime : time to wait before sending next command to the instruments. (max value of all instruments)
            resolution : This is the resolution of step size of the scan this must be larger than 10^n where n is the number of 
                        decimal places in the smallest step size of all axis.

        '''
        moduleType = '2DScan'    
        axes = ['X','Y']             
        startCoords = [8,13,0]      
        stopCoords = [10,14,0]     
        step_size =  [1,1,1]  
        scanStage = 'ScanStage'    
        scanReader = 'ScanOsc'       
        stageSettleTime = 0.5      
        resolution = 100          
        scanner = Scanner(moduleType,axes,startCoords,stopCoords,step_size,scanStage,scanReader,stageSettleTime,resolution)


* **NOTE: Do not make any changes in the section after this.**


* After the setup, save the config file and run the runExperiment.bat file.This will open console and start running the experiment.
