import numpy as np
import time
class dummyReader:
    def __init__(self,instrumentIp):
        self._ip = instrumentIp

    def set_acquisition_mode(self,mode):
        return 

    def set_number_averages(self,averages):
        return

    def set_acquisition_state(self,state):
        return

    def get_data(self,channel):
        time_base =np.arange(0,10000.00).astype(np.float32)
        ydata_in_units = np.random.uniform(low=0,high=0.5,size = 10000).astype(np.float32)
       
        return time_base, ydata_in_units 

    def query_all_acquisition(self):
        return 
class dummyMover:
    def __init__(self,serialNumber=None, HomeStage=False, stageName=None, stageType='linear', path_kinesis_install=r"C:\Program Files\Thorlabs\Kinesis"):
        
        return None


    def move_device(self,position):
        pass



