import time
import re
import math
import os
from ctypes import *
import numpy as np
class KinesisController:
    def __init__(self, serialNumber=None, HomeStage=False, stageName=None, stageType='linear', path_kinesis_install=r"C:\Program Files\Thorlabs\Kinesis"):

        os.chdir(path_kinesis_install)
        self.name = "C:\Program Files\Thorlabs\Kinesis\Thorlabs.MotionControl.KCube.DCServo.dll"
        self.stageName = stageName
        self.lib = cdll.LoadLibrary(self.name)
        self.type = stageType

        # Build device list
        self.lib.TLI_BuildDeviceList()

        self.serialNumber = serialNumber
        self.stepsPerRev = 512
        self.gearBoxRatio = 67
        if stageType == 'linear':
            self.pitch = 1
        elif stageType == 'rotational' or stageType == 'field' or stageType == 'power':
            self.pitch = 17.87
        else:
            print('Stage type undefined')

        self.moveTimeout = 60.0

        # Setting up stage
        print(f"Initializing {self.stageName}")
        self.initialize_device()

        # Adjusting stage parameters
        print(f"Adjusting {self.stageName} settings")
        self.set_up_device()

        # Homing stages
        if HomeStage == True:
            print(print(f"Homing {self.stageName}"))
            self.home_device()


    def initialize_device(self):
        # set up device
        self.lib.CC_Open(self.serialNumber)
        self.lib.CC_StartPolling(self.serialNumber, c_int(200))
        # might need to enable the channel:
        # lib.CC_EnableChannel(serialNumber)

        time.sleep(3)
        self.lib.CC_ClearMessageQueue(self.serialNumber)


    def clean_up_device(self):
        # clean up and exit
        self.lib.CC_ClearMessageQueue(self.serialNumber)
        # print(lib.CC_GetPosition())
        self.lib.CC_StopPolling(self.serialNumber)
        self.lib.CC_Close(self.serialNumber)


    def home_device(self):
        homeStartTime = time.time()
        self.lib.CC_Home(self.serialNumber)

        self.messageType = c_ushort()
        self.messageID = c_ushort()
        self.messageData = c_ulong()

        homed = False
        while (homed == False):
            self.lib.CC_GetNextMessage(self.serialNumber, byref(self.messageType), byref(self.messageID), byref(self.messageData))
            if ((self.messageID.value == 0 and self.messageType.value == 2) or (time.time() - homeStartTime) > self.moveTimeout):
                homed = True
        self.lib.CC_ClearMessageQueue(self.serialNumber)


    def set_up_device(self):
        # Set up to convert physical units to units on the device
        self.lib.CC_SetMotorParamsExt(self.serialNumber, c_double(self.stepsPerRev), c_double(self.gearBoxRatio), c_double(self.pitch))

        deviceUnit = c_int()
        self.deviceUnit = deviceUnit


    def move_device(self, position):
        deviceUnit = c_int()

        realUnit = c_double(position)

        self.lib.CC_GetDeviceUnitFromRealValue(self.serialNumber, realUnit, byref(deviceUnit), 0)

        moveStartTime = time.time()
        self.lib.CC_MoveToPosition(self.serialNumber, deviceUnit)

        moved = False
        messageType = c_ushort()
        messageID = c_ushort()
        messageData = c_ulong()
        while (moved == False):
            self.lib.CC_GetNextMessage(self.serialNumber, byref(messageType), byref(messageID), byref(messageData))

            if ((messageID.value == 1 and messageType.value == 2) or (time.time() - moveStartTime) > self.moveTimeout):
                moved = True

    def set_rel_efield(self, rel_efield):
        print("in rel_efield")
        angle = np.arccos(np.sqrt(rel_efield)) * 180 / math.pi
        self.move_device(position=angle)
    
    def set_rel_power(self, rel_power):
        angle = np.arccos(rel_power) * 180 / math.pi
        self.move_device(position=angle)


    def rotate_stage(self, angle):
        self.move_device(position=angle)



if __name__=="__main__":
    k = KinesisController(serialNumber=c_char_p(b'27255894'), HomeStage=True, stageName='Xaxis')
    k.move_device(2)
    k.clean_up_device()
