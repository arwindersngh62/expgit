

class Scanner():
    def __init__(self,axes,startCoords,stopCoords,steps,scanStage,scanReader,stageSettleTime,resolution,stageData):
        self._axis_num = len(axes) 
        self._nameMap = {}
        self._revNameMap = {}
        for stage in stageData:
            # map axis name to stage name
            self._nameMap[stage[3]] = stage[0]
        for stage in stageData:
            # map stage name to axis name
            self._revNameMap[stage[0]] = stage[3]
        print(self._nameMap)
        self._axes = axes
        self.startCoords = startCoords
        self.stopCoords = stopCoords
        self.steps = steps
        self.scanStage = scanStage
        self.scanReader = scanReader
        self.stageSettleTime =stageSettleTime
        self._resolution = resolution
        self._actions = []

        
    def getnameIndex(self,stage_name):
        
        return self._axes.index(self._revNameMap[stage_name])
    


    def scan_prelim(self,axis,actions):
        temp_actions =[]
        index = self._axes.index(axis)   ## index of the coordinates to be used for this scan
        startCoords = float(self.startCoords[index]) 
        stopCoords = float(self.stopCoords[index])
        steps = self.steps[index]
        
        temp_actions.append(Action(self.scanStage,'MoveStage',[self._nameMap[axis],[startCoords]]))
        temp_actions.append(Action('SYSTEM','Wait',[self.stageSettleTime]))
        
        
        for step in range(int((startCoords+steps)*self._resolution),int((stopCoords+steps)*self._resolution),int(steps*self._resolution)):
            temp_actions+=actions
            coord = step/self._resolution
            
            
            temp_actions.append(Action(self.scanStage,'MoveStage',[self._nameMap[axis],[step/self._resolution]]))
            temp_actions.append(Action('SYSTEM','Wait',[self.stageSettleTime]))
        temp_actions+=actions
        return temp_actions
    
        
    def add_read_actions(self):
        actions =[]
        actions.append(Action(self.scanReader,'SET_ACQ_STATE',['STOP']))
        actions.append(Action('SYSTEM','Wait',[0.5]))
        actions.append(Action(self.scanReader,'GET_DATA',[2]))
        actions.append(Action(self.scanReader,'SET_ACQ_STATE',['RUN']))
        return actions

    def scanND(self):
        dimensions = self._axis_num
        actions = self.add_read_actions()
        print(f"dimensions are {dimensions}")
        while dimensions>0:
            actions = self.scan_prelim(self._axes[dimensions-1],actions)
            dimensions-=1 
        
        #while sdimensions > 0:
         ##  sdimensions-=1
        return actions
  
    def get_actions(self):
        return (self._actions)

    def initiate(self):
        for axis in self._axes:
            index = self._axisMap[axis]
            self._actions.append(Action(self.scanStage,'MoveStage'
                            ,[self._nameMap[axis],self.startCoords[index]]))

    def compile(self):
        #self.initiate()
        self._actions += self.scanND() 
        for i in self._actions:
            print(i)

        return(self._actions)


if __name__ == "__main__":
    from action import Action
    axes = ['X','Y','Z']       #definition found in line 88ff  ,'Z', 'R1'    LEFTMOST APRAMETERS TURNS LAST
    startCoords = [1300,3,3,3]      
    stopCoords = [1200,1,1,1]     
    steps =  [100,-1,-1,-1]  
    scanStage = 'ScanStage'  
    scanReader = 'ScanOsc'       
    stageSettleTime = 0.5      
    resolution = 100   
    stageData = []
    stageData.append(['X-Stage','linear',27256338,'X',False]) 
    stageData.append(['Y-Stage','linear',27255894,'Y',False])  
    stageData.append(['Z-Stage','linear',27005114,'Z',False])
    stageData.append(['rotating','rotational',2700500,'R1',False])
    stageData.append(['wiregrid','rotational',7255354,'R2',False])
    scan = Scanner(axes,startCoords,stopCoords,steps,scanStage,
                    scanReader,stageSettleTime,resolution,
                    stageData)
    scan.compile()
else:
    from .action import Action

        
